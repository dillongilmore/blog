FROM debian:bookworm
RUN apt-get update && apt-get install -y git hugo make pandoc texlive-full curl
WORKDIR /srv/blog
