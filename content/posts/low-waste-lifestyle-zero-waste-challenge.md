---
title: Low Waste Lifestyle
date: 2018-12-28
tags:
  - sustainability
  - minimalism

---

I've lived in Seattle for almost a year now and I've made some
pretty big changes on how I spend my money, especially
groceries.

Last week, I intentionally didn't buy groceries until I ran
out of food so that I could go on a big grocery trip and
share how I buy food. Here is a picture of the results:

![Zero waste groceries](/img/groceries.jpg)

### Goals

Some people do zero-waste and I think that's great, but the
real problem is plastic. Plastic is extremely valuable and,
frankly, under-valued by the general population.

Compared to most materials, which have some sort of natural,
biodegradable process. Plastic is comparatively indestructible.
It is the only material that
doesn't break down due to weather and is completely resistant
to solvents (like stomach acid and biodiesel). Plastic can be
broken in halves many times, but it doesn't actually go away unless
you burn it or recycle it. Both of these processes are costly
and emit a lot of carbon. The best solution for petroleum is not
to use it.

Conversely, in your car, there
are a lot of synthetic parts that make your car last a lot longer.
In the medical community plastic is indispensable. Bio-plastics
can't really replace synthetic plastics due to petro-plastic's
ability to remain inert and non-reactive to the natural
world. I could go
on about the benefits of petroleum, and I hope those words
aren't taken out of context. The point is that petroleum is
extremely valuable and for
some reason the average life cycle of plastic to consumer lasts
about 14 minutes. Once in the landfill the plastic will sit
there for millions of years. More likely though, that plastic
will end up in waterways and the ocean and cause complete
devastation to our environment.

I am doing my best
to only support products and companies who either avoid plastic,
use plastic in a correct way or, better yet, avoid
petroleum altogether.

### Implementation

In Seattle, Pike's Place Market has a lot of great food that
doesn't come in plastic packaging. It's the only place where
I can find berries and spaghetti not wrapped in plastic.

I'm also extremely lucky to live about one mile from
[Central Coop](https://www.centralcoop.coop/) a grocery store
that sells a lot of bulk items.
I bring my jars and fill up on olive oil, honey, maple syrup,
and tea all without plastic containers.

A great online retailer is [Earth Hero](https://earthhero.com/).
They sell a lot of great tools for reducing your waste. They also
plant a tree and commit to being carbon neutral.

From Earth Hero I bought a
[Zero Waste box from Terracycle](https://earthhero.com/product-category/terracycle/)
. For anything that is not easily recyclable nor compostable I
can throw into the Zero Waste box.

### Composting

I am extremely lucky to live where I live. Seattle has a
composting program where they will take any food scraps and
anything else labeled as _compostable_ like paper and _some_
bio-plastics. I send about a full
bag per week of food scraps, which is the bulk of my waste.
This waste gets turned into compost that local farmers can
buy and turn into new crops. This is an example of sustainability
that I wish more companies would turn to.

### Recycling

Most plastic doesn't end up getting recycled. Even if they are
clean and have the little recycle symbol on it the chances
that plastic is actually recycled is extremely small. This
is why I will only really buy plastics from companies that
take back their plastic. [Lush](https://www.lushusa.com/) is
a great example of a
company that will take back their plastic. On top of that, Lush
commits to using all organic ingredients that you can actually
pronounce on top of fighting animal cruelty.

What is easily recyclable is paper. I don't have much issue with
taking paper and cardboard and throwing it in the recycling. The
chances are high that paper will be re-used in another product.
The key with paper is to look for the FSC symbol, which ensures
that paper products are being grown sustainability.
