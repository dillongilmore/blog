# Dyll.in
## Blog for https://dyll.in

This repo hosts the content and CI/CD for the website [dyll.in](https://dyll.in).

### Prereq's

I wasn't doing a good job of tracking all the deps.

- make
- hugo
- git
- curl

### Building

To do a production build just do:

```
make
```

To start the development server do:

```
make server
```

