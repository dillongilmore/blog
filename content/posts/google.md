---
Title: Thoughts on Google
Date: "2018-12-15"
hidden: true
Tags:
  - software
  - privacy

---

_Disclaimer:_ I am an advocate for better privacy protections.
As such, my views might be different, but I am always willing
to listen to new views and opinions in order to better my own
perspective.

That being said, I think Google has done some great things. In
this post, I would like to go over those good things as well as
point out where they could use improvement. Finally, I'll point
out what *I* think are acceptable services to use under Google's
umbrella and also point out which services to avoid altogether.

For a brief list of services I use check out
[this]("/posts/services") post.

## The Good

## The Bad

## tl;dr

Google's contribution to Linux and Open Source has forever
changed the technology landscape. They proved that open source
is so viable that we now have Microsoft contributing a ton of
their code to open source as well. I would argue that without
Google, Microsoft wouldn't have pushed for Linux or Open Source
as they have been doing in recent years. Maybe I'm wrong and
would enjoy a rebuttle if I am.

Supporting Google through GCP, in my opinion, would be something
I would vouch for. I think their services are top notch and
easy to use. On top of that, their pricing is a bit cheaper
compared to alternatives.

Besides that, I think all of Google's other products and services
should be avoided until (if) they start focusing more on
user experience and taking privacy more seriously.

