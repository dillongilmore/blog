#!emacs --script
;; Author: Dillon Gilmore <mail@dyll.in>
;; Commentary:
;;
;; WIP: These functions aren't working yet. Issues:
;; 1. Even though we set a buffer name it looks like org mode isn't able
;; to use it as a filename. Documentation on how to set buffer-base-buffer
;; is lacking
;; 2. Setting subtreep to t allows us to set the filename in the properties
;; of the file using :PROPERTIES:, but this causes other issues in the
;; export function, which always results in a fatal error.
;; 3. Setting +EXPORT_FILE_NAME technically works, but for some reason the
;; same code that works outside the export does not work inside the export.
;; The function is returning a string, but treating it as false during the
;; export.
;;
;; These 3 bugs above make it so we can't run the export function as part of
;; this script. Consider fixing these bugs and contributing the code back to
;; org-mode.

(defun re-search-forward-line
  (regexp)
  "Loop for the given regexp and return the line number"
  (re-search-forward regexp nil t 1)
  (count-lines 1 (point)))

(defun remove-frontmatter-generate-latex
  (filename)
  "Remove Hugo frontmatter before generating latex code"
  (erase-buffer)
  (insert-file-contents filename)
  (beginning-of-buffer)
  (let ((frontmatter-start (re-search-forward-line "^---$"))
        (frontmatter-end (re-search-forward-line "^---$")))
    (if (and (and frontmatter-start frontmatter-end)
              (< frontmatter-start frontmatter-end))
        (delete-region frontmatter-start frontmatter-end)))
  (org-mode)
  (org-latex-export-to-latex))

(defun remove-frontmatter-generate-latex-for-file
  (filename)
  "Take in a filename and create a new buffer from it before generating
latex"
  (set-buffer (create-file-buffer filename))
  (remove-frontmatter-generate-latex filename))

; This function lets us kind of re use the functions if we need to. If there
; is an unhandled argument then assume we want to run these functions with it
(let ((filename (elt command-line-args-left 0)))
  (if filename
      (remove-frontmatter-generate-latex-for-file filename)))
