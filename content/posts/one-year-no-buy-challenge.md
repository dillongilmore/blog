---
title: The One Year Nothing New Challenge
date: 2018-12-28
lastmod: 2020-01-01
tags:
  - minimalism

---

I've lived in Seattle for almost a year. I bought my ticket
from San Jose to Seattle about a year ago for March 1st 2018.
It was the start of a
new life for me. I remember being extremely stressed because
I had to go through ALL of my possessions and decide what to
keep and what to sell and donate away.
It was surprisingly easy for me
to detach from the physical items. I was extremely relieved
when it was mostly gone. I ended up keeping about four boxes and
two suit cases worth of things and I had some good friends
who helped me store those boxes until I could get to them.

Since then I have acquired new stuff. Almost none of the new
things I have were copied from my old stuff. I truly didn't
need the things I gave away and I realized there were other
things that I have gained so much value from. Yet I find
myself spending quite a lot of money and now that it's been
a year I am fed up with my consumerism tendencies and have
decided to go on a one year fast.

I will be making a budget to live by and starting on
March 1st, 2019 I will not buy anything new until
March 1st, 2020. I will budget for things like bills, food and
travel. I will not buy any new clothes, shoes or other things
that will enable me to have new experiences until March 1st,
2020.

The only exception I will allow is I will be documenting the
things that I think I need and allow my friends and/or family
to buy things for me as birthday and/or Christmas presents.

Until March 1st, 2019 I am figuring out the things I need and
am buying them now.

The goal is to see how much money I could live on. If I lost
my job tomorrow then what changes to my life would I need to make
to live on the littlest amount of money possible. I will
be publishing my budget and keep track of how much I actually
spent. I'll also be allowing rollover cash so if I spend too
much money during one month I'll need to figure out how to
save for the next month and vice versa.

## 2020: Update

This quickly fell off the radar. Ultimately, it's not about
the things. I simply prioritized my relationship more than I
prioritized not having things. The things help augment life,
and somewhere along the way I pendulumed between things are
bad and things are good. It turns out that things are just
things and they don't have an alighment. You simply need to
remember that life is ephemeral and not to get too entrenched
in one ideology or another.

