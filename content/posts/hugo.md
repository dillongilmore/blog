---
Title: "Thought process for an SSG"
Date: "2018-12-15"
Lastmod: "2024-01-03"
Tags:
  - software
---

I spent a lot of time researching different solutions for a blog before
deciding to keep it simple. I was obsessed with a few qualities that I
later decided weren't important.

These qualities were:

1. Static site on first load
2. Allow JavaScript to be turned off
3. Evolve into a progressive web app with asynchronous loading

I used this [site](https://www.staticgen.com/) to find and compare
different solutions.

In particular, I've evaluated: Cryogen, Jekyll, Next, Gatsby, and Hugo. After
trying to use Next and Gatsby I decided that they were just too complicated
and my new list of important features became:

1. Static site
2. Allow JavaScript to be turned off
3. Ease of use
4. Will be maintained in long-term future

Based off of \#3 I immediately removed Next off the list, and the combination of
\#2 with \#3 removes Gatsby. I think those frameworks are certainly good, but for
what I'm doing they are overkill. I was tempted to not look at Jekyll either
because I've had horror stories with RubyGems, but for the sake of science I'll
actually include all projects on the Static Gen site (including Next/Gatsby
even though I won't be picking them).

With Hugo and Pandoc supporting _org-mode_, I can write all of my notes, blog posts,
resume's, etc in a single markup language and convert them to HTML, PDF, or even
man pages. I, generally, stick to markdown as it's certainly the simplest of
markups, but there's a huge power with org-mode that I'm slowly starting to master.

