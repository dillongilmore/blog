---
Title: Softwares as a Services
Date: "2018-12-15"
hidden: true
Tags:
  - software
  - privacy

---

Here's a list of servies I think are worth checking out. I either
use or have used all of these and find them to be quite viable
in practice.

## Objectives

1. Focus on privacy

Software as a service is inherintly a privacy abusing paradigm.
There isn't any way to verify that a certain provider isn't
abusing their service. A service could say that they encrypt
everything and that they don't sell or share your data, but
that might not necessarily be true. You simply have to take their
word for it.

That being said, the services that do have a focus on privacy
don't seem to have any other thing going for them. For example,
if someone found out that ProtonMail was leaking emails to
advertisers it would be the complete end of ProtonMail. This
is because that's their entire pitch. My conclusion of
ProtonMail is that they are, indeed, respecting your privacy,
but again is only because I simply trust them and I don't have
any way of actually verifying that.

## Email

- ProtonMail: As mentioned before, I have had a great experience
with ProtonMail. I enjoy their UI and use their bridge with
mutt. I have to pay for it, but it makes sense since they aren't
making money from any other source.

## Hosting

- Linode: I am currently a subscriber to Linode and have been
for at least a decade, if not more.

