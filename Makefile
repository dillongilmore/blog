#
# Makefile settings
#
SHELL := /bin/bash

#
# Convenience variables
#
DEBUG ?= 0
BRANCH ?= main
NAMESPACE ?= dillongilmore

#
# Paths
#
BUILD := build
STATIC := static

# theme
THEME := hugo-theme-pickles
THEME_DIR := themes/$(THEME)
TAR_DEST := $(BUILD)/$(THEME).zip
HUGO_THEME_URL := https://gitlab.com/$(NAMESPACE)/$(THEME)/-/archive/$(BRANCH)/hugo-theme-pickles-$(BRANCH).zip

# Content
CONTENT := content
BUILT_CONTENT := $(BUILD)/$(CONTENT)
ORG_FILES := $(wildcard $(CONTENT)/*.org) $(wildcard $(CONTENT)/**/*.org)

# Pandoc
PANDOC_FORMATS := pdf odt docx epub
__pandoc_format = $(foreach format,$(PANDOC_FORMATS),$(patsubst $(CONTENT)/%.$1,$(STATIC)/$(format)/%.$(format),$2))
PANDOC_FILES := $(call __pandoc_format,org,$(ORG_FILES))

#
# Binaries with optimal flags for later usage. Also provides a good list of dependencies.
#
UNARCHIVER ?= unzip -o
FETCH ?= curl -sL
HUGO ?= hugo
PANDOC ?= pandoc
EMACS ?= emacs
MK ?= make
SED ?= sed
DOCKER ?= docker

# Development dependencies and binaries
INOTIFY ?= inotifywait

# Dependencies of dependencies
#LATEX_TO_PDF ?= pdflatex

#
# Helpful functions
#
define mkdirp =
@[ -d $1 ] || mkdir -p $1
endef

#
# All other targets
#

#
# all: Alias for hugo-html (the default target)
#
.PHONY: all
all: hugo

#
# hugo-html: Turn markdown/org-mode to HTML
#
.PHONY: hugo-html
hugo: deps assets hugo-build

hugo-build:
	$(HUGO) -t $(THEME)

#
# ${TAR_DEST}: If our tarballs don't exist then download them, else ignore
#
$(TAR_DEST):
	$(call mkdirp,$(BUILD))
	$(FETCH) $(HUGO_THEME_URL) > $(TAR_DEST)

#
# ${THEME_DIR}: Download and unpackage theme
#
$(THEME_DIR):
	$(call mkdirp,$(@D))
	cd $(@D); \
		$(UNARCHIVER) $(CURDIR)/$(TAR_DEST); \
		mv $(THEME)-$(BRANCH) $(THEME)

#
# deps: Fetch any and all dependencies that we will need for building
# 		the site.
#
.PHONY: deps
deps: $(TAR_DEST) $(THEME_DIR)

# Re-usable rule for the `assets` target
define __pandoc_rule
$(STATIC)/$1/%.$1: $(CONTENT)/%.org
	$(call mkdirp,$$(@D))
	$(PANDOC) -o $$@ $$<
endef
$(foreach format,$(PANDOC_FORMATS),$(eval $(call __pandoc_rule,$(format))))

#
# assets: Build local assets
#
assets: $(PANDOC_FILES)

#
# clean: If you need to start from scratch
#
.PHONY: clean
clean:
	$(foreach format,$(PANDOC_FORMATS), \
		rm -rf $(STATIC)/$(format)/;)
	rm -rf $(BUILD)
	rm -rf $(THEME_DIR)
	rm -f $(TAR_DEST)
	rm -rf public/

#
# server: Start the hugo development server with optimal flags. This
# 		  target runs in the foreground and will not return unless you
# 		  send C-c.
#
.PHONY: server
.EXPORT_ALL_VARIABLES: server
server: hugo-html
	source scripts/server.sh && server

#
# Run a build in docker in order to test what Gitlab build is doing
#
docker:
	$(DOCKER) build -t debian-hugo-emacs .
	$(DOCKER) run -v $(CURDIR):/srv/blog debian-hugo-emacs $(MK)

#
# Helper target to ourput all of our computed variables
#
.PHONY: debug
debug:
	@echo "$(PANDOC_FILES)"

