---
Title: About
Date: 2019-03-17
Lastmod: 2024-01-03
Hidden: true
Tags:
  - software

---

Hello there! I only recommend reading this if you are suffering from
extreme boredom. If you are a potential employer there is a link to my
résumé at the bottom of this page. Anyways, here's a breakdown of
what I've done and the places I've been.

## July 1993 - San Diego, California

I was born in San Diego to a mother, father and two older brothers. Six years later
my sister would join us. I, generally, tell people that us siblings raised
ourselves. We are more like each other than we are like our parents. Without
my siblings my entire world would be completely different.

I'm not sure when exactly this would have happened, but when I was around 13 years
old my Brother would have been around 20 years old. He's created for himself an
extremely
successful career on his own, and I knew that I wanted to do what he was doing.
The problem was that I didn't have any idea what he was doing.

Later on, he handed me his Perl Programming book after I expressed interest in
learning to program. I read the entire book while
typing in programs on an HP _netbook_ running some very early version of Ubuntu. I
remember being over at my brother's various apartments over the years going into
deep discussion about language theory and playing video games. These are some of
my best memories from my childhood.

Eventually, I would have written my own templating library, and when my brother
saw what I was doing he introduced me to Template Toolkit 2 as well as the vast
library that is CPAN.

At around the same time, Google App Engine was just released and my brother
encouraged me to build out an app in Python. So, I did. It was a simple
app that tallied votes for a Google Image search. It would randomize the
images that came out of the search. For example, you could type in
_New York Pizza_ and _Chicago Pizza_. You would vote on which images looked
better and after 10 votes it would reveal what you believe perceived to be
better.

It was a stupid, but simple app that was my introduction to full stack
programming. I learned vanilla AJAX, jQuery/DOM, Python, and databases. I
made it responsive and showed it to people at my high school. Quickly, people
were doing all kinds of voting on the app and crowds of people would hover over
and debate on which images were better.

When I turned 16, I was eager to join the workforce and ended up in Fast Food
and then child care where I ended up wasting a few years of my life until I
turned 19. I really wanted to do an internship at a place where I could
write software, but San Diego wasn't the place for me. My grandparents offered
me room and board if I went to school full-time so I went to go live with them
in Las Vegas.

## August 2012 - Las Vegas, Nevada

Once in Vegas, I enrolled in school to get a degree in Electrical Engineering.
I felt that if I was to be engaged with school that I should avoid software, and
hardware seemed like a good compliment. I got pretty good grades, and in the
meantime I hunted for somewhere to write software.

A local man ended up finding my post on Craigslist and seemed interested in
hiring me. I pestered him daily on what I could do to prove to him that I was
capable to work on whatever project he needed me to do. He wanted to see that
I could write a WordPress plugin. 12 hours later of pure focus and
Mountain Dew, I ended up showing him what I could do. He called me over
and on December 1st 2012 I started at my first Software gig. I was ecstatic.
This job would end up changing my entire life as it was the first time I
would be able to acquire relevant experience.

I worked long and hard hours for the next year. I was passionate about proving
that I could do the work. Initially, I did some WordPress development,
but my boss saw that very quickly I needed more work thrown at me. So, he
put me on a team of 4 where we developed an MVC PHP web application that
was essentially the MailChimp of Skype called Skysponder. After a few months
of working on this project I noticed a lot of very bad coding practices
from the lead developer at the time. After a couple of more months my boss
removed him from the project and I was now referred to as the Lead
Developer only after working on the project for a few months. To this day,
I am very proud of the hard work I put into this project.

At school, we were forced to take a software class. My initial attempt was to
test out of the class. The issue was that half of the questions weren't about
software. The other half were relevant software questions, but _the answer
key was wrong_. For example, the answer key had the wrong delimiters on the
for loop as well as the wrong answer for what the _double_ type was. I believe
there were other incorrect answers, but my memory is failing to remember what
they were. I explicitly remember the _for_ loop question and the _double_
question because when I raised these issues with the teachers and told them
I knew for a fact what
a for loop was and what the double type is. Instead of a productive
conversation it just turned into an argument. I tried to pull up documentation
on the internet, but at the end of the day I was _just a student_ and I
didn't know more than the teachers. I made an attempt to take the class,
but what I noticed was the teacher wasn't allowing anyone to be
creative with code. It was either his way or it was wrong, even when it
was right. I noticed a lot of other students were frustrated by this, so
after the first day I dropped out of school.

At this point in time my brother was working at Google and living in San Jose.
I looked around at rent prices and determined it was time to move. I
packed all of my belongings into my car and headed to San Jose.

## October 2013 - San Jose, California

My brother let me stay at his place for a week. I needed to find a new
apartment and in the meantime still working on Skysponder.

Luckily, I was able to find an apartment in under a week. I was
20 years old at the time and was stoked that I could have my own place
all on my own.

After about a month of living in San Jose, I decided that I wanted
a local job and it didn't take long for
recruiters to place me at VentureBeat. I quickly surprised most of
the team how good I was at tracking down bugs and getting projects
done quickly. Unfortunately, my time here didn't last long because
VentureBeat was running out of money and they had to cut development
resources. They really wanted me to stay on because they had projects
lined up in the next few months, but I needed a company that could
commit and I also had rent to pay.

After working with more recruiters they eventually placed me at
Shipwire. I was shocked with the offer they gave me. My experience
was very low, but I later learned that they were impressed by
my drive and how much knowledge I did have even though my
experience was minimal.

After starting, I felt like I had renewed energy. I wasn't placed on
a team, but was left in a mostly empty sandbox to rebuild their
marketing platform. The best part of this job wasn't necessarily
the problem set, but instead the great people that worked here. I
learned a lot of things from many people in my first year.

I knew that I wanted to live in San Francisco so after my lease was
up I moved one hour north into the city.

## March 2014 - San Francisco, California

Only a couple of interesting things happened while I lived in
San Francisco. The first is that I ended up rewriting the
marketing platform we were using and invented a system where a
single repository could host multiple sites based off of simple
configuration. Pages would be put together using a templating
system where a single element could generate hundreds of lines of
HTML, CSS, and JavaScript. This allowed up to bring up new sites
and pages extremely quickly, which was what allowed us to keep
up pace in the very fast moving world of Marketing.

The second thing that happened was I entered the first serious
relationship of my life. Things moved quickly, and we wanted
to move in together. I wanted to stay in San Francisco, but
unfortunately I didn't think my studio would be a great place
to hold the possessions of two people. So, we found something
more economical somewhere else.

## March 2016 - Mountain View, California

After moving to Mountain View there are two significant events that
happened in my life.

The first event that happened was that I was bored again. When I get
bored I tend to find new work, but unexpectedly a certain individual
at Shipwire left the company. I had been asking to be in that
particular position and my company quickly moved me to fill the
role of DevOps. I wouldn't be working on Marketing projects anymore.

For me, this was pretty exciting. I had been talking about a lot of
projects I would like to work on if I was on the DevOps team and
now that opportunity was in front of me. I developed many systems
in many languages using many platforms and frameworks. The learning
experience on this team will always be valuable to me.

My relationship with my partner at the time was great up until it
ended. I'm still not sure
why it ended, but it did. Suddenly, I was single and heart broken
and my brothers had ended up moving to Washington.

I had been working at Shipwire for 4 years and had a great support
network to help me through the tough times. I kept hearing the
same thing over and over again from some of the best friends
I have had in my life. I ended up deciding that I should follow
my family and go to Seattle.

## March 2018 - Seattle, Washington

I'm not sure what it is about Seattle, but something just _feels_
right. I enjoy being here every day.

I ended up transitioning to full time remote for Shipwire. I didn't
plan on leaving the company as I still had a ton of projects I
wanted to finish before I was ready to leave. I subscribed
to WeWork and was as passionate as ever about all of the new
technology coming out.

Unfortunately, at the same time, some of the greatest people I've
ever met were starting to leave the company. I knew I would miss
those people, but I also knew that it wasn't reason enough to
leave as well.

Since thinking that, I've learned the valuable lesson of _people
mean everything_. I didn't have any clue how spoiled I was to have
such a great management team until they were gone.

At the same time, I have had the realization that I want to
help people. Not just people, but also the planet. I believe it's
up to normal individuals to do their part in making sure the
decisions they make aren't creating negative impacts on society.

With those facts in place I am ready for the next endeavor that
can fill me with renewed passion.

