#!/usr/bin/env bash
#
# NOTE: This file is currently pure, which means you can source it into a
# running shell and it won't make any changes. It will only give access to
# new functions.
#

# we are running the hugo server in the background so that we can use
# the foreground process to wait for file changes ourselves
PID=""

# TODO: If the hugo server is running then kill it, we will recompile ourselves
# and start a new server. It would be nice if there was an option to run a server
# like this that doesn't trigger recompiles since we want to do that ourselves.
cleanup() {
  if [ $PID ]; then
    kill -1 $PID

    # We are sending a sighup, not a sigkill or sigterm, which means we allow
    # the hugo server to do cleanup. In case the process is still running we
    # should wait for it to finish before continuing.
    wait $PID

    # In case we run this function again we want to make sure the condition works
    # correctly.
    PID=""
  fi
}

start() {
  cleanup

  # recompile everything including PDF assets etc
  ${MK}

  # We are really only running this because it supplies a basic HTTP server to
  # show our assets.
  ${HUGO} server --port 10102 \
          --theme ${THEME} \
          --disableFastRender &

  # Track the pid of the hugo process
  PID=$!
}

#
# hugo-server: wrapper around the _hugo server_ command so that
#              we can compile other assets prior to running the server
#
server() {
  #set -e

  # If we C-c or something else bad happens still cleanup processes. This
  # should be unnecessary as, generally, when parent processes die their
  # children are cleaned up, but we should try to avoid defunct processes
  # if possible.
  #trap 'cleanup' EXIT

  # after running make we will be making file changes, which will trigger
  # inotify, but we don't want to be running this loop over and over again
  # when it hasn't finished so we put this lock here just in case
  local lock=0

  # When we start up this function we don't expect to make any modifications
  # immediately so we can premptively start the server _then_ start inotify.
  start

  # NOTE: this loop should only be running once and the lock here isn't really
  # proven to be required. It shouldn't be required, but it's still here just in case
  while inotifywait -r ${ORG} -r ${CONTENT} ${PDF_OUT} \
    -e delete -e create -e modify \
    --excludei "[a-zA-Z/0-9-]+\\.#.*\\..*"; do
    if [ $lock -eq 0 ]; then
      lock=1
    fi

    start

    # Release our arbitrary lock
    if [ $lock -eq 0 ]; then
        lock=0
    fi
  done
}

