---
title: 2023 year in review and 2024 goals
date: 2024-01-03
tags:
  - year-in-review
  - goals
  - personal

---

# 2023

## Abroad

I visited Vietnam in January and had the chance to also travel to Singapore
and Thailand on the same trip. It was the first time I travelled to Asia
on a non-work trip. I was there for 3 weeks and worked every Friday to keep
on top of email, messages and address any issues holding up projects. I was
surprised by how fast and stable internet connections were as I travelled
between countries and even a few hours away from any major cities.

We visited during the Vietnamese new year (also knows as Lunar or Chinese new
year),and the entire country was in celebration mode.
Eating mung bean sticky rice every single day, which I'm apparently addicted to
now. Fireworks on the eve of new year's and I got to (illegally) drive the
motor scooters around town (10/10 experience). It helps to visit a country
where your spouse natively understands the language. We had such a good
time that by the time we left we were already planning the next visit.

## Home Projects

Right after our trips abroad we had some construction work on our home's
foundation, which seemed to go well until we noticed water leaking it the
next week. After a ton of dramatic back and forth we finally settled out
of court to get it fixed and replace all of our damaged flooring. We then
redid all the floors by leveling them followed by laying down LVP.

Soon after, in June we had Sarah's parents come visit us and they began
renovating our backyard. It's hard to describe just how good that went.
The backyard went from an eyesore to a place that I want to spend time
everyday. After making it look nice we proceeded to build a covered deck,
which we completed in 3 days. It involved every single member of the family
to come over and do the build and painting.

## At REI

The year had a lot of promise. I began as the lead engineer for REI's
_Observability Platform_, which is _Enterprise_ speak for all things
monitoring. It was fascinating trying to teach an old enterprise
new tricks.

I had an opportunity to have an interview with a company that I had
been eyeing that would have given me an opportunity to work on robots,
and sustainability but because the appeal of being in charge, leading a
team and having the final say in all matters was far too difficult to
give up. REI is also a company that I've enjoyed working for, tremendously.

I switched to work for REI because I didn't want to work for a soulless
company, and that was true, for the most part. What I didn't realize is
that at heart, I am an engineer who wants to work on difficult and
challenging problems and _any_ job involved with retail, marketing, or
logistics just isn't going to fulfill me in a sustainable way.

# 2024

## Abroad

We've already booked a 3+ week trip to Japan where we'll visit the northern half
of the country including: Tokyo, Kyoto and some of the surrounding cities.
We've talked about going to Japan for some time, and shocked how much more
expensive it will be compared to Vietnam.

## Home Projects

I'm, very slowly, building a shed from scratch with nothing by wood, and Youtube.
I'm hoping this year is light on projects as last year was quite exhausting. The
only major issue that I want to fix is redirecting roof water to go around the
house rather than under it.

## Career

I plan on spending some serious time looking around. I want to be able to spend
my energy and time on something where I can be creative and proud of my work.
I've already started putting in some time to refresh on C/C++ and completed
[AoC](https://gitlab.com/dillongilmore/aoc).

Positions that I've seen so far that interest me:

### Nvidia DCGM: Software Engineer

I find this to be _sustainability-adjacent_. A CPU maxed out will use about
300 watts and a graphics card maxed out will also use about 300 watts. The
major difference is the 300 watts from the graphics card can complete about
50x+ of work than the CPU (if optimized correctly). It'll be interesting to
see where that technology goes but ultimately the more workloads that can move
to GPU's will be better for the environment. Not only will the watt usage be
lower but the amount of raw materials required to setup and leverage GPU's
are lower. The only thing that remains constant is cooling.

### FirstMode: Software Engineer

This company is why I started learning C/C++ in the first place. Even though
I've been coding in Python and Java for 5 years now, I saw that their
Software Engineer position is in the embedded space so I picked up an Arduino
and began tinkering on some small projects.

It turns out that they are also in cloud and their cloud role for Software
Engineer is also something I could excel at on day 1.

This has been a company that I've been eyeing for a long time, and combines
both my passion for technology and sustainability into a single awesome package.

### evGO: Software Engineer

I'm far more excited about this company. It would be a dream to help bring the
electric revolution to the world. I could see myself at this company for a very
long time. I was surprised to find out that I already have all of the skills
required to fulfill their Software Engineer role which is doubly exciting.

### LevelTen Energy

They work on renewable energy and any role in that industry is where I want to be.
Their software engineer position isn't too different from evGO's so I have
and I think that this would be an awesome place to work.



